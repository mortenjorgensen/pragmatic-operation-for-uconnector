﻿using UConnector.Api.V1;

namespace UConnector.Samples.Operations
{
    public class PragmaticObjectOperation : Operation
    {
        protected override IOperation BuildOperation()
        {
            return FluentOperationBuilder
                .Receive<Pragmatic.PragmaticObjectReciever>()
                .Debatch()
                .Transform<Pragmatic.PragmaticObjectTransformer>()
                .Batch()
                .Send<Pragmatic.PragmaticObjectWriter>()
                .ToOperation();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UConnector.Samples.Pragmatic
{
    public interface IPragmaticObjectEditor
    {
        void UpdateObject(IEnumerable<int> id);
    }
}

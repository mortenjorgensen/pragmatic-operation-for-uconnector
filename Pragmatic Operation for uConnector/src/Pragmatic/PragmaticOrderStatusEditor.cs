﻿using System.Collections.Generic;
using System.Linq;
using UCommerce.EntitiesV2;
using UCommerce.Transactions;
using UConnector.Config;
using ObjectFactory = UCommerce.Infrastructure.ObjectFactory;

namespace UConnector.Samples.Pragmatic
{
    public class PragmaticOrderStatusEditor : IPragmaticObjectEditor
    {
        public void UpdateObject(IEnumerable<int> id)
        {
            var orderService = ObjectFactory.Instance.Resolve<IOrderService>();

            var newOrderStatus = new OrderStatus(int.Parse(GetConfigurationValue("PragmaticObjectOperation", "PragmaticObjectWriter", "NewOrderStatus")));

            var purchaseOrders = id.Select(i => PurchaseOrder.Find(x => x.OrderId == i).First()).ToList();

            foreach (var order in purchaseOrders)
            {
                orderService.ChangeOrderStatus(order, newOrderStatus);
                order.Save();
            }
        }

        private string GetConfigurationValue(string operationName, string configurationName, string optionName)
        {
            var path = new OperationConfigurationReader();

            foreach (var operationSectionItem in path.GetOperationConfigurations())
            {
                if (operationSectionItem.Section.Name.Contains(operationName))
                {
                    var temp = operationSectionItem;
                    var anotherTemp = temp.Section.Configs.AddOrGet(configurationName);
                    return anotherTemp.Options.AddOrGet(optionName, true).Value;
                }
            }
            return "";
        }
    }
}

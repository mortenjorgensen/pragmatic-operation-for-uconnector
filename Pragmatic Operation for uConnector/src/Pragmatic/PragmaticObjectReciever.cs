﻿using System;
using System.Collections.Generic;
using System.Linq;
using UCommerce.EntitiesV2;
using UConnector.Config;
using UConnector.Framework;
using UConnector.Samples.Pragmatic.Datatypes;
using UConnector.Samples.Pragmatic.Datatypes.Base;

namespace UConnector.Samples.Pragmatic
{
    public class PragmaticObjectReciever : IReceiver<IEnumerable<PragmaticTransformable>>
    {
        public IEnumerable<PragmaticTransformable> Receive()
        {
            return Read();
        }

        private IEnumerable<TransformablePurchaseOrder> Read()
        {
            var data = new List<TransformablePurchaseOrder>();
            var orderIds = new List<int>();

            var purchaseOrders =
                PurchaseOrder.Find(x => x.OrderStatus.OrderStatusId == int.Parse(GetConfigurationValue("PragmaticObjectOperation", "PragmaticObjectReciever", "OrderStatus")));

            foreach (var order in purchaseOrders)
            {
                var transformable = new TransformablePurchaseOrder();
                var purchaseOrder = order;
                var shipments = Shipment.Find(x => x.PurchaseOrder.OrderId == purchaseOrder.OrderId);

                foreach (var shipment in shipments)
                {
                    transformable.TemplateDefinition =
                    string.Format(
                        GetConfigurationValue("PragmaticObjectOperation", "PragmaticObjectReciever",
                                              "TemplateFileName"), shipment.ShipmentName);

                    transformable.OutputFolderName =
                        String.Format(GetConfigurationValue("PragmaticObjectOperation", "PragmaticObjectReciever",
                                                            "OutputFolderName"),
                                      shipment.ShippingMethod.ShippingMethodId);

                    transformable.OutputFileName =
                        String.Format(GetConfigurationValue("PragmaticObjectOperation", "PragmaticObjectReciever",
                                                            "OutputFileName"), order.OrderId);

                    transformable.KeyValuePairs = GetPropertyValues(order);

                    transformable.ObjectIdentifier = order.OrderId;

                    data.Add(transformable);
                    
                    transformable = new TransformablePurchaseOrder();
                }

                orderIds.Add(order.OrderId);
                
            }

            IPragmaticObjectEditor editor = new PragmaticOrderStatusEditor();
            editor.UpdateObject(orderIds);

            return data;
        }

        private string GetConfigurationValue(string operationName, string configurationName, string optionName)
        {
            var path = new OperationConfigurationReader();

            foreach (var operationSectionItem in path.GetOperationConfigurations())
            {
                if (operationSectionItem.Section.Name.Contains(operationName))
                {
                    var item = operationSectionItem;
                    var conf = item.Section.Configs.AddOrGet(configurationName);
                    return conf.Options.AddOrGet(optionName, true).Value;
                }
            }
            return "";
        }

        private IEnumerable<string> GetPropertyValues(PurchaseOrder input)
        {
            var shipment = Shipment.Find(x => x.PurchaseOrder.OrderId == input.OrderId).First();

            var orderAddress = OrderAddress.Find(x => x.PurchaseOrder.OrderId == input.OrderId && x.AddressName == "Shipment").First();

            var country = orderAddress.Country;

            var temp = new HashSet<string>();

            var properties = new List<Object>
                {
                    country,
                    input,
                    orderAddress,
                    OrderLine.Find(x => x.PurchaseOrder.OrderId == input.OrderId).First(),
                    Customer.Find(x => x.CustomerId == input.Customer.CustomerId).First(),
                    shipment,
                    ShippingMethod.Find(x => x.ShippingMethodId == shipment.ShippingMethod.ShippingMethodId).First()
                    
                };


            foreach (var o in properties)
            {
                foreach (var property in o.GetType().GetProperties())
                {
                    try
                    {
                        temp.Add("[" + property.Name + "],[" + property.GetValue(o, null) + "]");
                    }
                    catch (Exception)
                    {
                        temp.Add("");
                    }
                }

            }

            return temp;
        }
    }
}

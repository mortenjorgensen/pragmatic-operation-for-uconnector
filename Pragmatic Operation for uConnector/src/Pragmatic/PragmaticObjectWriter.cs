﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UConnector.Config;
using UConnector.Framework;
using UConnector.Samples.Pragmatic.Datatypes.Base;

namespace UConnector.Samples.Pragmatic
{
    public class PragmaticObjectWriter : ISender<IEnumerable<PragmaticTransformable>>
    {
        public void Send(IEnumerable<PragmaticTransformable> input)
        {
            foreach (var pragmaticTransformable in input)
            {
                var path = GetConfigurationValue("PragmaticObjectOperation", "PragmaticObjectWriter", "Directory") +
                           pragmaticTransformable.OutputFolderName + @"\";

                if (Directory.Exists(path) == false)
                {
                    Directory.CreateDirectory(path);
                }

                var fileName = pragmaticTransformable.OutputFileName + pragmaticTransformable.OutputFileType;

                var file = new FileStream(path + fileName, FileMode.OpenOrCreate, FileAccess.Write);

                bool isWriten;
                try
                {
                    using (var stream = new StreamWriter(file, Encoding.GetEncoding("ISO-8859-1")))
                    {
                        stream.WriteLine(pragmaticTransformable.OutputToFile);
                    }

                    isWriten = true;
                }
                catch (Exception)
                {
                    isWriten = false;
                }
            }
        }

        private string GetConfigurationValue(string operationName, string configurationName, string optionName)
        {
            var path = new OperationConfigurationReader();

            foreach (var operationSectionItem in path.GetOperationConfigurations())
            {
                if (operationSectionItem.Section.Name.Contains(operationName))
                {
                    var item = operationSectionItem;
                    var conf = item.Section.Configs.AddOrGet(configurationName);
                    return conf.Options.AddOrGet(optionName, true).Value;
                }
            }
            return "";
        }
    }
}

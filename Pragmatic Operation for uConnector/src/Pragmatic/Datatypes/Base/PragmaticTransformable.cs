﻿using System.Collections.Generic;

namespace UConnector.Samples.Pragmatic.Datatypes.Base
{
    public abstract class PragmaticTransformable
    {
        //Contains property name and value. Syntax is: [Name][Value]
        public abstract IEnumerable<string> KeyValuePairs { get; set; }
        
        //Defines the name of desired template file.
        public abstract string TemplateDefinition { get; set; }
        
        //Defines the output folder name.
        public abstract string OutputFolderName { get; set; }
        
        //Holds the string to be written to output file. Is set in the transformer.
        public abstract string OutputToFile { get; set; }
        
        //Defines the output file type. Is the same as the template file type.
        public abstract string OutputFileType { get; set; }
        
        //Defines the output file name.
        public abstract string OutputFileName { get; set; }
        
        //Identifier to be used in IPragmaticObjectEditor.
        public abstract int ObjectIdentifier { get; set; }
    }
}
